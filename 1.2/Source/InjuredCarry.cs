﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using UnityEngine;
using Verse.AI;
using HarmonyLib;
using System.Reflection;

namespace InjuredCarry
{
    [StaticConstructorOnStartup]
    internal static class HarmonyInit
    {
        static HarmonyInit()
        {
            new Harmony("Haecriver.InjuredCarry").PatchAll();
        }
    }

    public static class Patches
    {
        private static TargetingParameters ForInjured(Pawn pawn)
        {
            return new TargetingParameters
            {
                canTargetPawns = true,
                canTargetBuildings = false,
                neverTargetIncapacitated = true,
                neverTargetHostileFaction = true,
                validator = delegate (TargetInfo targ)
                {

                    if (!targ.HasThing)
                    {
                        return false;
                    }
                    Pawn injuredPawn = targ.Thing as Pawn;
                    if (
                        injuredPawn == null // There is no pawn
                        || injuredPawn == pawn // Self
                        || (injuredPawn.RaceProps.Animal && injuredPawn.Faction == null) // Is a wild animal
                    )
                    {
                        return false;
                    }
                    return HealthAIUtility.ShouldSeekMedicalRest(injuredPawn);
                }
            };
        }

        [HarmonyPatch(typeof(FloatMenuMakerMap), "AddHumanlikeOrders")]
        public static class FloatMenuMakerCarryAdder
        {
            [HarmonyPostfix]
            public static void AddFloatMenuOption(Vector3 clickPos, Pawn pawn, List<FloatMenuOption> opts)
            {
                if (pawn.health.capacities.CapableOf(PawnCapacityDefOf.Manipulation))
                {
                    foreach (LocalTargetInfo localTargetInfo1 in GenUI.TargetsAt_NewTemp(clickPos, ForInjured(pawn), true))
                    {
                        Pawn victim = (Pawn)localTargetInfo1.Thing;
                        if (!victim.InBed() && pawn.CanReserveAndReach(victim, PathEndMode.OnCell, Danger.Deadly, 1, -1, null, ignoreOtherReservations: true))
                        {
                            if (!victim.IsPrisonerOfColony
                                && (!victim.InMentalState || victim.health.hediffSet.HasHediff(HediffDefOf.Scaria))
                                && (victim.Faction == Faction.OfPlayer || victim.Faction == null)
                            )
                            {
                                opts.Add(FloatMenuUtility.DecoratePrioritizedTask(new FloatMenuOption("Rescue".Translate(victim.LabelCap, victim), delegate
                                {
                                    Building_Bed building_Bed = RestUtility.FindBedFor(victim, pawn, sleeperWillBePrisoner: false, checkSocialProperness: false);
                                    if (building_Bed == null)
                                    {
                                        building_Bed = RestUtility.FindBedFor(victim, pawn, sleeperWillBePrisoner: false, checkSocialProperness: false, ignoreOtherReservations: true);
                                    }
                                    if (building_Bed == null)
                                    {
                                        string t3 = (!victim.RaceProps.Animal) ? ((string)"NoNonPrisonerBed".Translate()) : ((string)"NoAnimalBed".Translate());
                                        Messages.Message("CannotRescue".Translate() + ": " + t3, victim, MessageTypeDefOf.RejectInput, historical: false);
                                    }
                                    else
                                    {
                                        Job job = JobMaker.MakeJob(DefDatabase<JobDef>.GetNamed("CarryInjured"), victim, building_Bed);
                                        job.count = 1;
                                        pawn.jobs.TryTakeOrderedJob(job);
                                        PlayerKnowledgeDatabase.KnowledgeDemonstrated(ConceptDefOf.Rescuing, KnowledgeAmount.Total);
                                    }
                                }, MenuOptionPriority.RescueOrCapture, null, victim), pawn, victim));
                            }
                        }
                    }
                }
            }
        }
    }
}
