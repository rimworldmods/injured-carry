﻿//inspired by https://gist.github.com/erdelf/84dce0c0a1f00b5836a9d729f845298a
using System.Collections.Generic;
using Verse;
using UnityEngine;

namespace InjuredCarry
{
    public class InjuredCarrySettings : ModSettings
    {
        /// <summary>
        /// The three settings our mod has.
        /// </summary>
        public bool rescueNotInjured = false;
       

        /// <summary>
        /// The part that writes our settings to file. Note that saving is by ref.
        /// </summary>
        public override void ExposeData()
        {
            Scribe_Values.Look(ref rescueNotInjured, "InjuredCarry.Settings.RescueNotInjured", false, true);
        
            base.ExposeData();
        }
    }

    public class InjuredCarry : Mod
    {
        /// <summary>
        /// A reference to our settings.
        /// </summary>
        InjuredCarrySettings settings;

        /// <summary>
        /// A mandatory constructor which resolves the reference to our settings.
        /// </summary>
        /// <param name="content"></param>
        public InjuredCarry(ModContentPack content) : base(content)
        {
            this.settings = GetSettings<InjuredCarrySettings>();
        }

        /// <summary>
        /// The (optional) GUI part to set your settings.
        /// </summary>
        /// <param name="inRect">A Unity Rect with the size of the settings window.</param>
        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(inRect);
            listingStandard.CheckboxLabeled(
                "InjuredCarry.Settings.RescueNotInjured.Desc".Translate(),
                ref settings.rescueNotInjured
            );
            listingStandard.End();
            base.DoSettingsWindowContents(inRect);
        }

        /// <summary>
        /// Override SettingsCategory to show up in the list of settings.
        /// Using .Translate() is optional, but does allow for localisation.
        /// </summary>
        /// <returns>The (translated) mod name.</returns>
        public override string SettingsCategory()
        {
            return "InjuredCarry".Translate();
        }
    }
}