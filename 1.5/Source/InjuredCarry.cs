﻿using System.Collections.Generic;
using RimWorld;
using Verse;
using UnityEngine;
using Verse.AI;
using HarmonyLib;
using System;
using RimWorld.Planet;
using Verse.AI.Group;
using System.Reflection;

namespace InjuredCarry
{
    [StaticConstructorOnStartup]
    internal static class HarmonyInit
    {
        static HarmonyInit()
        {
            new Harmony("Haecriver.InjuredCarry").PatchAll();
        }
    }

    public static class Patches
    {
        public static Type VehiclePawnType = AccessTools.TypeByName("Vehicles.VehiclePawn");

        private static TargetingParameters ForInjured(Pawn pawn)
        {
            return new TargetingParameters
            {
                // Classic parameters
                canTargetPawns = true,
                canTargetBuildings = false,
                mapObjectTargetsMustBeAutoAttackable = false,

                // Override classic parameters
                neverTargetIncapacitated = true,

                // Precising parameters
                neverTargetHostileFaction = true,
                onlyTargetFactions = new List<Faction>() { Faction.OfPlayer, null },
                validator = delegate (TargetInfo targ)
                {

                    if (!targ.HasThing)
                    {
                        return false;
                    }
                    Pawn injuredPawn = targ.Thing as Pawn;
                    if (
                        injuredPawn == null // There is no pawn
                        || injuredPawn == pawn // Self
                        || (injuredPawn.RaceProps.Animal && injuredPawn.Faction == null) // Is a wild animal
                        || (injuredPawn.RaceProps.Humanlike && injuredPawn.Faction == null) // Is wild human
                    )
                    {
                        return false;
                    }

                    // Check for vehicle
                    if (VehiclePawnType != null && VehiclePawnType.IsAssignableFrom(targ.Thing.GetType()))
                    {
                        return false;
                    }

                    // Override injuries if option is activated
                    if (LoadedModManager.GetMod<InjuredCarry>().GetSettings<InjuredCarrySettings>().rescueNotInjured)
                    {
                        return true;
                    }

                    return HealthAIUtility.ShouldSeekMedicalRest(injuredPawn);
                }
            };
        }

        [HarmonyPatch(typeof(FloatMenuMakerMap), "AddHumanlikeOrders")]
        public static class FloatMenuMakerCarryAdder
        {
            private static void ValidateTakeToBedOption(Pawn pawn, Pawn target, FloatMenuOption option, string cannot, GuestStatus? guestStatus = null)
            {
                MethodInfo methodInfo = typeof(FloatMenuMakerMap).GetMethod("ValidateTakeToBedOption", BindingFlags.NonPublic | BindingFlags.Static);
                var parameters = new object[] { pawn, target, option, cannot, guestStatus };
                methodInfo.Invoke(null, parameters);
            }

            private static bool WantsToBeRescued(Pawn pawn)
            {
                // remove down condition
                if (pawn.InBed() || pawn.IsCharging())
                {
                    return false;
                }
                if (CaravanFormingUtility.IsFormingCaravanOrDownedPawnToBeTakenByCaravan(pawn))
                {
                    return false;
                }
                if (pawn.IsMutant && !pawn.mutant.Def.entitledToMedicalCare)
                {
                    return false;
                }
                if (pawn.InMentalState && !pawn.health.hediffSet.HasHediff(HediffDefOf.Scaria))
                {
                    return false;
                }
                if (pawn.ShouldBeSlaughtered())
                {
                    return false;
                }
                if (pawn.TryGetLord(out var lord) && lord.LordJob is LordJob_Ritual lordJob_Ritual && lordJob_Ritual.TryGetRoleFor(pawn, out var role)) // remove down ritual need here
                {
                    return false;
                }
                // remove always downed lifestage
                return true;
            }

            public static bool CanRescueNow(Pawn rescuer, Pawn patient)
            {
                // remove forced conditions
                if (!WantsToBeRescued(patient))
                {
                    return false;
                }
                if (!rescuer.CanReserveAndReach(patient, PathEndMode.OnCell, Danger.Deadly, 1, -1, null, ignoreOtherReservations: true))
                {
                    return false;
                }
                return true;
            }


            [HarmonyPostfix]
            public static void AddFloatMenuOption(Vector3 clickPos, Pawn pawn, List<FloatMenuOption> opts)
            {
                if (pawn.health.capacities.CapableOf(PawnCapacityDefOf.Manipulation))
                {
                    foreach (LocalTargetInfo localTargetInfo1 in GenUI.TargetsAt(clickPos, ForInjured(pawn), thingsOnly: true))
                    {
                        Pawn victim = (Pawn)localTargetInfo1.Thing;
                        if (!CanRescueNow(pawn, victim) || victim.mindState.WillJoinColonyIfRescued)
                        {
                            continue;
                        }
                        TaggedString taggedString = ((HealthAIUtility.ShouldSeekMedicalRest(victim) || !victim.ageTracker.CurLifeStage.alwaysDowned) ? "Rescue".Translate(victim.LabelCap, victim) : "PutSomewhereSafe".Translate(victim.LabelCap, victim));
                        FloatMenuOption floatMenuOption = FloatMenuUtility.DecoratePrioritizedTask(
                            new FloatMenuOption(taggedString, delegate
                        {
                            Building_Bed building_Bed = RestUtility.FindBedFor(victim, pawn, checkSocialProperness: false);
                            if (building_Bed == null)
                            {
                                building_Bed = RestUtility.FindBedFor(victim, pawn, checkSocialProperness: false, ignoreOtherReservations: true);
                            }
                            if (building_Bed == null)
                            {
                                string text = ((!victim.RaceProps.Animal) ? ((string)"NoNonPrisonerBed".Translate()) : ((string)"NoAnimalBed".Translate()));
                                Messages.Message("CannotRescue".Translate() + ": " + text, victim, MessageTypeDefOf.RejectInput, historical: false);
                            }
                            else
                            {
                                // dont forget its a different job
                                Job job = JobMaker.MakeJob(DefDatabase<JobDef>.GetNamed("CarryInjured"), victim, building_Bed);
                                job.count = 1;
                                pawn.jobs.TryTakeOrderedJob(job, JobTag.Misc);
                                PlayerKnowledgeDatabase.KnowledgeDemonstrated(ConceptDefOf.Rescuing, KnowledgeAmount.Total);
                            }
                        }, MenuOptionPriority.RescueOrCapture, null, victim), pawn, victim);
                        string key = (victim.RaceProps.Animal ? "NoAnimalBed" : "NoNonPrisonerBed");
                        string cannot = string.Format("{0}: {1}", "CannotRescue".Translate(), key.Translate().CapitalizeFirst());
                        ValidateTakeToBedOption(pawn, victim, floatMenuOption, cannot);
                        opts.Add(floatMenuOption);
                    }
                }
            }
        }
    }
}
